#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import re
import threading

import requests
import wikipedia as wiki
from assistant import Assistant, read_json_file

# ======================================================================================================================

assist = Assistant()
abbrevs = None
file_path = os.path.dirname(os.path.realpath(__file__)) + "/"
request_site_id = "default"


# ======================================================================================================================

def query_wiki(word):
    """ Run a query with wikipedia and prepare result """

    try:
        # Run query
        max_chars = int(assist.get_config()["secret"]["max_result_length"])
        query_chars = max_chars + 200
        result = wiki.summary(word, chars=query_chars, auto_suggest=True, redirect=True)

        # Delete special characters
        result = result.split("==")[0]
        result = re.sub('\s+', ' ', result)
        result = re.sub('[^A-Za-z0-9ÄÖÜäöüßẞ ()\[\]%€$.?!,-]+', '', result)

        # Remove texts in brackets
        result = re.sub('\([^)]*\)', '', result)
        result = re.sub('\[[^\]]*\]', '', result)

        # Replace abbreviations
        pattern = re.compile(r'\b(' + '|'.join(abbrevs.keys()) + r')')
        result = pattern.sub(lambda x: abbrevs[x.group()] if x.group() in abbrevs else x.group(), result)

        # Remove multiple whitespaces
        result = re.sub('\s+', ' ', result)

        # Try to get a description not to long and not to short
        result = re.sub('[?!]+', '.', result)
        splitted = result.split(". ")
        for i in reversed(range(1, len(splitted))):
            result = ". ".join(splitted[0:i])
            if (len(result) < max_chars):
                break

        result = assist.get_text("result").format(word) + " " + result + "."

    except requests.exceptions.ConnectionError:
        result = assist.get_text("no_connection")
    except wiki.exceptions.PageError:
        result = assist.get_text("no_article_found").format(word)
    except wiki.exceptions.DisambiguationError as e:
        result = assist.get_text("to_much_articles").format(word)
        for i in range(3):
            if (len(e.options) > i):
                result += " " + e.options[i] + "."

    return result


# ======================================================================================================================

def get_wiki_response(word):
    """ Wait for wikipedia to respond and say result """

    result_sentence = query_wiki(word)
    h = assist.get_hermes()
    h.publish_start_session_notification(request_site_id, result_sentence, None)


# ======================================================================================================================

def callback_query(word, site_id):
    """ Callback to handle query request """
    global request_site_id

    # Extra non blocking thread
    thread = threading.Thread(target=get_wiki_response, args=(word,))
    thread.start()

    request_site_id = site_id
    result_sentence = assist.get_text("wait_for_response")
    return result_sentence


# ======================================================================================================================

def callback_query_by_letters(hermes, intent_message):
    if (intent_message.slots is not None and len(intent_message.slots.letters) >= 1):

        word = []
        for val in intent_message.slots.letters:
            word.append(val.slot_value.value.value)

        word = "".join(word)
        word = word.replace("<space>", " ")
        result_sentence = callback_query(word, intent_message.site_id)

    else:
        result_sentence = assist.get_text("asked_word_not_understood")

    hermes.publish_end_session(intent_message.session_id, result_sentence)


# ======================================================================================================================

def callback_query_word(hermes, intent_message):
    if (intent_message.slots is not None and len(intent_message.slots.word) >= 1):

        word = str(intent_message.slots.word.first().value)
        if (word != "unknownword"):
            result_sentence = callback_query(word, intent_message.site_id)
            hermes.publish_end_session(intent_message.session_id, result_sentence)
            return

    result_sentence = assist.get_text("asked_word_not_understood") + " " + assist.get_text("ask_for_spelling")
    hermes.publish_continue_session(intent_message.session_id, result_sentence, ["kleintody:spelled_word"])


# ======================================================================================================================

wiki.set_lang(assist.get_config()["secret"]["language"])
abbrevs = read_json_file(file_path + "abbreviations.json")
abbrevs = abbrevs[assist.get_config()["secret"]["language"]]

if __name__ == "__main__":
    h = assist.get_hermes()
    h.connect()
    h.subscribe_intent("kleintody:asked_word", callback_query_word)
    h.subscribe_intent("kleintody:spelled_word", callback_query_by_letters)
    h.loop_forever()
